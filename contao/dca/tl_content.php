<?php

declare(strict_types=1);

$GLOBALS['TL_DCA']['tl_content']['fields']['spacingMargin'] = [
    'inputType'             => 'select',
    'eval'                  => ['tl_class' => 'w50 w50h autoheight', 'multiple' => true, 'size' => '10', 'chosen' => true, 'mandatory' => false],
    'sql'                   => "text NULL"
];

$GLOBALS['TL_DCA']['tl_content']['fields']['spacingPadding'] = [
    'inputType'             => 'select',
    'eval'                  => ['tl_class' => 'w50 w50h autoheight', 'multiple' => true, 'size' => '10', 'chosen' => true, 'mandatory' => false],
    'sql'                   => "text NULL"
];
