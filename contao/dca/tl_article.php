<?php

declare(strict_types=1);

use Contao\CoreBundle\DataContainer\PaletteManipulator;

$GLOBALS['TL_DCA']['tl_article']['fields']['articleSpacing'] = [
    'inputType'             => 'select',
    'eval'                  => ['tl_class' => 'w50 w50h autoheight', 'multiple' => true, 'size' => '10', 'chosen' => true, 'mandatory' => false],
    'sql'                   => "text NULL"
];

PaletteManipulator::create()
    ->addField('articleSpacing', 'article_legend', PaletteManipulator::POSITION_APPEND)
    ->applyToPalette('default', 'tl_article')
;

