<?php

declare(strict_types=1);

namespace designerei\ContaoSpacingBundle\Classes;

use designerei\ContaoTailwindBridgeBundle\Tailwind\TailwindClasses;
use designerei\ContaoTailwindBridgeBundle\Tailwind\Utilities\Margin;
use designerei\ContaoTailwindBridgeBundle\Tailwind\Utilities\Padding;

class OptionsCallback
{
    private $tailwindClasses;
    private $margin;
    private $padding;

    public function __construct(
        TailwindClasses $tailwindClasses,
        Margin          $margin,
        Padding         $padding
    ) {
        $this->tailwindClasses  = $tailwindClasses;
        $this->margin           = $margin;
        $this->padding          = $padding;
    }

    public function getMarginClasses()
    {
        return $this->tailwindClasses->mergeClasses([
            $this->margin->getMarginTopClasses(),
            $this->margin->getMarginRightClasses(),
            $this->margin->getMarginBottomClasses(),
            $this->margin->getMarginLeftClasses()
        ]);
    }

    public function getPaddingClasses()
    {
        return $this->tailwindClasses->mergeClasses([
            $this->padding->getPaddingTopClasses(),
            $this->padding->getPaddingRightClasses(),
            $this->padding->getPaddingBottomClasses(),
            $this->padding->getPaddingLeftClasses()
        ]);
    }
}