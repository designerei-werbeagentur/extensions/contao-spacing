<?php

declare(strict_types=1);

namespace designerei\ContaoSpacingBundle\ContaoManager;

use designerei\ContaoSpacingBundle\ContaoSpacingBundle;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\CoreBundle\ContaoCoreBundle;
use designerei\ConplateHelpersBundle\ConplateHelpersBundle;

class Plugin implements BundlePluginInterface
{
    public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(ContaoSpacingBundle::class)
                ->setLoadAfter([
                    ContaoCoreBundle::class,
                    ConplateHelpersBundle::class
                ]),
        ];
    }
}
