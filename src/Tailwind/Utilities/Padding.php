<?php

declare(strict_types=1);

namespace designerei\ContaoSpacingBundle\Tailwind\Utilities;

use Contao\CoreBundle\DependencyInjection\Attribute\AsHook;
use designerei\ContaoTailwindBridgeBundle\Tailwind\TailwindClasses;
use designerei\ContaoTailwindBridgeBundle\Tailwind\Safelist;

class Padding
{
    private object $tailwindClasses;
    private object $safelist;
    private array  $spacing;

    public function __construct(
        object $tailwindClasses,
        object $safelist,
        array  $spacing
    ) {
        $this->tailwindClasses = $tailwindClasses;
        $this->safelist = $safelist;
        $this->spacing = $spacing;
    }

    public function getPaddingBasicClasses(): array
    {
        $prefix = 'p';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getPaddingLeftRightClasses(): array
    {
        $prefix = 'px';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getPaddingTopBottomClasses(): array
    {
        $prefix = 'py';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getPaddingTopClasses(): array
    {
        $prefix = 'pt';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getPaddingBottomClasses(): array
    {
        $prefix = 'pb';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getPaddingRightClasses(): array
    {
        $prefix = 'pr';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getPaddingLeftClasses(): array
    {
        $prefix = 'pl';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getPaddingClasses(): array
    {
        return $this->tailwindClasses->mergeClasses([
            $this->getPaddingBasicClasses(),
            $this->getPaddingLeftRightClasses(),
            $this->getPaddingTopBottomClasses(),
            $this->getPaddingTopClasses(),
            $this->getPaddingRightClasses(),
            $this->getPaddingBottomClasses(),
            $this->getPaddingLeftClasses()
        ]);
    }

    #[AsHook('initializeSystem')]
    public function __invoke(): void
    {
        $this->safelist->addToSafelist($this->getPaddingClasses());
    }
}