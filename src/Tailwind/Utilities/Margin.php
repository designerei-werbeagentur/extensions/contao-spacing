<?php

declare(strict_types=1);

namespace designerei\ContaoSpacingBundle\Tailwind\Utilities;

use Contao\CoreBundle\DependencyInjection\Attribute\AsHook;
use designerei\ContaoTailwindBridgeBundle\Tailwind\TailwindClasses;
use designerei\ContaoTailwindBridgeBundle\Tailwind\Safelist;

class Margin
{
    private $tailwindClasses;
    private $safelist;
    private $spacing;

    public function __construct(
        object $tailwindClasses,
        object $safelist,
        array $spacing
    ) {
        $this->tailwindClasses = $tailwindClasses;
        $this->safelist = $safelist;
        $this->spacing = $spacing;
    }

    public function getMarginBasicClasses(): array
    {
        $prefix = 'm';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getMarginLeftRightClasses(): array
    {
        $prefix = 'mx';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getMarginTopBottomClasses(): array
    {
        $prefix = 'my';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getMarginTopClasses(): array
    {
        $prefix = 'mt';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getMarginBottomClasses(): array
    {
        $prefix = 'mb';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getMarginRightClasses(): array
    {
        $prefix = 'mr';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getMarginLeftClasses(): array
    {
        $prefix = 'ml';
        $suffixes = $this->spacing;

        return $this->tailwindClasses->generateClasses($prefix, $suffixes);
    }

    public function getMarginClasses(): array
    {
        return $this->tailwindClasses->mergeClasses([
            $this->getMarginBasicClasses(),
            $this->getMarginLeftRightClasses(),
            $this->getMarginTopBottomClasses(),
            $this->getMarginTopClasses(),
            $this->getMarginRightClasses(),
            $this->getMarginBottomClasses(),
            $this->getMarginLeftClasses()
        ]);
    }

    #[AsHook('initializeSystem')]
    public function __invoke(): void
    {
        $this->safelist->addToSafelist($this->getMarginClasses());
    }
}