<?php

declare(strict_types=1);

namespace designerei\ContaoSpacingBundle\DataContainer;

use Contao\CoreBundle\DependencyInjection\Attribute\AsCallback;

class SetOptionsListener
{
    private $marginOptions;
    private $paddingOptions;

    public function __construct(
        $marginOptions,
        $paddingOptions,
    )
    {
        $this->marginOptions  = $marginOptions;
        $this->paddingOptions = $paddingOptions;
    }

    #[AsCallback(table: 'tl_content', target: 'fields.spacingMargin.options')]
    public function setMarginOptions(): array
    {
        return $this->marginOptions->getMarginClasses();
    }

    #[AsCallback(table: 'tl_content', target: 'fields.spacingPadding.options')]
    public function setPaddingOptions(): array
    {
        return $this->paddingOptions->getPaddingClasses();
    }

    #[AsCallback(table: 'tl_article', target: 'fields.articleSpacing.options')]
    public function SetArticleSpacingOptions(): array
    {
        return array_merge_recursive(
            $this->paddingOptions->getPaddingClasses(),
            $this->marginOptions->getMarginClasses()
        );
    }
}
