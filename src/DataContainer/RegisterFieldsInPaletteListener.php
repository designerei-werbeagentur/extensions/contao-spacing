<?php

declare(strict_types=1);

namespace designerei\ContaoSpacingBundle\DataContainer;

use Contao\CoreBundle\DependencyInjection\Attribute\AsCallback;
use Contao\DataContainer;

class RegisterFieldsInPaletteListener
{
    #[AsCallback(table: 'tl_content', target: 'config.onload')]
    public function __invoke(DataContainer $dc = null): void
    {
        foreach ($GLOBALS['TL_DCA']['tl_content']['palettes'] as $k => $palette) {
            if (!\is_array($palette) && false !== strpos($palette, 'cssID')) {
                $GLOBALS['TL_DCA']['tl_content']['palettes'][$k] = str_replace(
                    '{expert_legend',
                    '{spacing_legend:hide},spacingMargin,spacingPadding;{expert_legend',
                    $GLOBALS['TL_DCA']['tl_content']['palettes'][$k]
                );
            }
        }
    }
}

