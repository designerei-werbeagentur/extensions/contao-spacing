# Contao-Spacing

## Function & usage

This extension for Contao Open Source CMS allows you to define margin and padding for content elements.

## Configuration

This is the default configuration of the options. You can configure these options by adjusting the `config.yml` file.

Note: The rendered class structure is based on Tailwind CSS.

```yml
contao_tailwind:
  default_classes:
    margin: foo
    padding:
      - foo
      - bar
  spacing:
    - foo
    - bar
    - yaz
```